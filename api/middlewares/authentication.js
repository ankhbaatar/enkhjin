const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {
    try {
        const token =
            req.cookies.accessToken || req.query.accessToken || req.headers['x-access-token'];
        if (!token) {
            console.error('undefined accessToken');
            res.error.Forbidden('authentication.error');
            return;
        }

        const tokenData = jwt.verify(token, process.env.AUTH_KEY);
        if (!tokenData.myHID) {
            console.error('undefined user_id');
            res.json('bad_token');
            return false;
        }
        req.myInfo = {
            id: parseInt(tokenData.myHID, 10),
            token,
            comId: tokenData.myComId,
            key: tokenData.key,
            server: tokenData.server,
        };
        next();
    } catch (e) {
        res.error.Forbidden('authentication.error');
    }
};

module.exports = auth;
