const { validationResult } = require('express-validator');

module.exports = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        return next();
    }

    // console.log(errors);
    const extractedErrors = [];
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }));

    // throw new Error({
    //     type: 'validation',
    //     errors: extractedErrors,
    // });
    return res.error.Validate('validation.error', {
        errors: extractedErrors,
    });
};
