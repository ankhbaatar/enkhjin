const authentication = require('./authentication');
const authorization = require('./authorization');
const validation = require('./validation');
// const modelAutoBinding = require('./modelAutoBinding');

module.exports = {
    authentication,
    authorization,
    validation,
    // modelAutoBinding,
};
