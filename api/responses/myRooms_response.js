const { getUserInfo, decodeStr } = require('../../helpers/constFunc');
const { decode } = require('../../helpers/crypt');

module.exports = async (roomList, myInfo, db, accessToken) => {
    let myRoomsInfo = null;
    const { mysql, redis } = db;
    const users = [];
    const myRooms = [];
    roomList.map(async (roomData, i) => {
        // if (room.users.length === 2) {
        const room = roomData.result;
        room.users.map(userRoom => {
            const userExs = users.findIndex(userID => userID === userRoom.id);
            if (userExs === -1) {
                users.push(userRoom.id);
            }
            return userRoom;
        });
        myRooms.push(room);
        // }
    });
    // const resWorker = await requestCloudCenter(ids, myInfo);
    const resWorker = await getUserInfo(users, redis, mysql, {
        accessToken,
        valueType: 'userId',
    });
    const onlineUsers = await redis.get('newChatableOnlineUsers');
    const objectArray = onlineUsers ? Object.entries(onlineUsers) : [];
    const myList = myRooms.map(room => {
        const roomUser = room.users.find(user => user.id !== myInfo.id);
        if (room.users.length === 2 && room.roomType !== 1) {
            objectArray.map(([key, onlineUser]) => {
                if (roomUser && roomUser.id) {
                    if (roomUser.id === onlineUser.id) {
                        room.isOnline = true;
                    }
                }

                return onlineUser;
            });
            if (roomUser) {
                const userData = resWorker.find(obj => {
                    return parseInt(obj.id, 10) === parseInt(roomUser.id, 10);
                });
                if (userData) {
                    room.path = userData.user_icon;
                    room.name = userData.system_name;
                }
            }
        } else {
            const groupOnline = room.users.find(
                user =>
                    user.id !== myInfo.id &&
                    objectArray.find(([key, onlineUser]) => onlineUser.id === user.id)
            );
            if (groupOnline) {
                room.isOnline = true;
            }
        }
        room.users.map((roomUserInfo, indexUser) => {
            const userData = resWorker.find(
                obj => parseInt(obj.id, 10) === parseInt(roomUserInfo.id, 10)
            );
            room.users[indexUser].app_name = '';
            room.users[indexUser].system_name = '';
            room.users[indexUser].phone = '';
            room.users[indexUser].status = '';
            room.users[indexUser].user_icon = '';
            if (userData) {
                room.users[indexUser] = Object.assign(room.users[indexUser], userData);
            }
            return roomUserInfo;
        });
        const myUser = room.users.find(user => user.id === myInfo.id);
        room.hasNew = false;
        if (room.lastMsgInfo.lastFrom !== myInfo.id) {
            if (myUser) {
                if (new Date(room.lastMsgInfo.createdAt) > new Date(myUser.last_seen)) {
                    room.hasNew = true;
                }
            }
        }
        room.lastMsgInfo.lastMsg = decode(room.lastMsgInfo.lastMsg);
        return room;
    });
    // console.log(' room.duuslaa', myList);
    myRoomsInfo = {
        result: myList,
    };

    return myRoomsInfo;
};
