const roomsResponse = require('./rooms_response');
const { requestCloudCenter } = require('../../helpers/functions');
const { values } = require('../../config/response');
const { getUserInfo } = require('../../helpers/constFunc');

module.exports = async (myRooms, myInfo, db) => {
    let myRoomsInfo = null;
    let ids = '';
    const { mysql, redis } = db;

    // console.log('myRooms.result.length', myRooms.result.length);
    myRooms.result.map(async (room, i) => {
        if (room.users.length === 2) {
            const roomUser = room.users.find(user => user.id !== myInfo.id);
            ids += `${roomUser.id},`;
        }
    });

    ids = ids.substring(0, ids.lastIndexOf(','));
    // const resWorker = await requestCloudCenter(ids, myInfo);
    const resWorker = await getUserInfo(ids, redis, mysql);
    const myList = myRooms.result.map(room => {
        if (room.users.length === 2) {
            const roomUser = room.users.find(user => user.id !== myInfo.id);
            const userData = resWorker.data.data.usersList.find(
                obj => parseInt(obj.id, 10) === parseInt(roomUser.id, 10)
            );
            if (userData) {
                room.path = userData.user_icon;
                room.name = userData.system_name;
            }
        }
        const myUser = room.users.find(user => user.id === myInfo.id);
        if (myUser) {
            console.log(new Date(room.lastMsgInfo.createdAt) + '>=' + new Date(myUser.last_seen));
            if (new Date(room.lastMsgInfo.createdAt) >= new Date(myUser.last_seen)) {
                room.hasNew = true;
            } else {
                room.hasNew = false;
            }
        }

        return room;
    });
    // console.log(' room.duuslaa', myList);
    myRoomsInfo = {
        result: myList,
    };

    return myRoomsInfo;
};
