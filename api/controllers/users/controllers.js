const CryptoJS = require('crypto-js');
const bcrypt = require('bcryptjs');
const moment = require('moment');
const {
    getUserInfo,
} = require('../../../helpers/constFunc');
const Mongo = require('../../../models/mongo');

let usersModel = Mongo.users.model;

module.exports.createUser = async (req, res, next) => {
    try {
        
        const { name,pass } = req.body;
        const result = await usersModel.create({
            userName: name,
            userPass: pass,
        });
        res.success.Created('roomInfo', result);
    } catch (err) {
        console.log('err', err);
        res.success.Created('roomInfo', err);
    }
};

module.exports.getList = async (req, res, next) => {
    try {
        
        const { name } = req.query;
        const result = await usersModel.find({userName:name});
        res.success.Created('roomInfo', result);
    } catch (err) {
        console.log('err', err);
        res.success.Created('roomInfo', err);
    }
};