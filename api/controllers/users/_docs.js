// ---- getList
/**
 * @swagger
 * /firebase:
 *   get:
 *     tags: ["Firebase"]
 *     summary: Өрөөний жагсаалт авах api
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *         - in: query
 *           name: userId
 *           schema:
 *               type: string
 *               required: false
 *         - in: query
 *           name: isWeb
 *           schema:
 *              type: boolean
 *              required: false
 *     responses:
 *       404:
 *         description: notFound
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  message:
 *                         type: string
 *                         description: Error message.
 *                         example: authentication.error
 *                  code:
 *                         type: integer
 *                         description: Error code.
 *                         example: 404
 *       200:
 *         description: lists авсан бол array дотор json буцаана
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  id:
 *                         type: integer
 *                         description: The user ID.
 *                         example: 1
 *                  phone:
 *                         type: string
 *                         description: The user phone.
 *                         example: 86088806
 *                  avatarPath:
 *                         type: string
 *                         description: The user phone.
 *                         example: https://bytelinks-icard.s3.ap-northeast-2.amazonaws.com/avatar_6.png
 *                  createdAt:
 *                         type: string
 *                         format: date
 *                         description: The user phone.
 *                         example: 2021-01-01
 *                  updatedAt:
 *                         type: string
 *                         format: date
 *                         description: The user phone.
 *                         example: 2021-01-01
 */

// ---- getOne
/**
 * @swagger
 * /firebase/details:
 *   get:
 *     tags: ["Firebase"]
 *     summary: Нэг хэрэглэгчийг авах api
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       404:
 *         description: 'bearer token буруу эсвэл байхгүй бол authentication.error мессэж буцаана'
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  message:
 *                         type: string
 *                         description: Error message.
 *                         example: authentication.error
 *                  code:
 *                         type: integer
 *                         description: Error code.
 *                         example: 404
 *       200:
 *         description: success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  id:
 *                         type: integer
 *                         description: The account ID.
 *                         example: 1
 *                  phone:
 *                         type: string
 *                         example: 86088806
 *                  avatarPath:
 *                         type: string
 *                         example: https://bytelinks-icard.s3.ap-northeast-2.amazonaws.com/avatar_6.png
 *                  createdAt:
 *                         type: string
 *                         format: date
 *                         example: 2021-01-01
 *                  updatedAt:
 *                         type: string
 *                         format: date
 *                         example: 2021-01-01
 */

// ---- Үүсгэх
/**
 * @swagger
 * /firebase:
 *   post:
 *     tags: ["Firebase"]
 *     summary: шинэ өрөө үүсгэх api
 *     security: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               phone:
 *                 type: string
 *                 example: "99101010"
 *               password:
 *                 type: string
 *                 example: "12345678"
 *
 *     responses:
 *       422:
 *         description: 'Утасны дугаар, и-мейл String биш, нууц үг 8-с доош оронтой бол validation.error мессэж буцаана'
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  message:
 *                         type: string
 *                         description: Error message.
 *                         example: validation.error
 *                  errors:
 *                         type: array
 *                         description: Error message.
 *                         example: '{"password": "Invalid value"}'
 *                  code:
 *                         type: integer
 *                         description: Error code.
 *                         example: 422
 *                  status:
 *                         type: string
 *                         description: status.
 *                         example: error
 *       404:
 *         description: 'Утасны дугаар давхардаж байвал user duplicated мессэж буцаана'
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  message:
 *                         type: string
 *                         description: Error message.
 *                         example: user duplicated
 *                  code:
 *                         type: integer
 *                         description: Error code.
 *                         example: 404
 *                  status:
 *                         type: string
 *                         description: status.
 *                         example: error
 *       201:
 *         description: success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  token:
 *                      type: string
 *                      example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDA5NWUxYWRiYWJjZWNkNTRlZDRjNzUiLCJwaG9uZSI6Ijg2MDg4ODA2IiwidWFnZW50IjoiUG9zdG1hblJ1bnRpbWUvNy4yNi4xMCIsImlhdCI6MTYxMTM4MzYyMX0.7Fgyh14UbIlS9fmmlk22A97tmE9jSRojfSD9Jyp1Xwk
 *                  status:
 *                         type: string
 *                         description: status
 *                         example: success
 */

// ---- update
/**
 * @swagger
 * /firebase/:
 *   put:
 *     tags: ["Firebase"]
 *     summary: Өрөөг update хийх api
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               type:
 *                 type: integer
 *                 example: 0
 *               email:
 *                 type: string
 *                 example: "bodio@gmail.com"
 *               firstName:
 *                 type: string
 *                 example: "bodi"
 *               lastName:
 *                 type: string
 *                 example: "bat"
 *               avatarPath:
 *                 type: string
 *                 example: "bat"
 *     responses:
 *       200:
 *         description: success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  status:
 *                         type: string
 *                         description: status.
 *                         example: success
 *
 */

// ---- delete
/**
 * @swagger
 * /firebase/:
 *   delete:
 *     tags: ["Firebase"]
 *     summary: Өрөөг delete хийх api
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       '200':
 *         description: success
 *         content:
 *           application/json:
 *             schema:
 *                  type: integer
 *                  example: 1
 */
