const Router = require('express').Router();

const { authentication } = require('../../middlewares');
const asyncRouteWrapper = require('../../../helpers/asyncRouteWrapper');
const controllers = require('./controllers.js');

Router.post.apply(Router, [
    '/createUser',
    // authentication,
    // ...validations.update,
    asyncRouteWrapper(controllers.createUser),
]);

Router.get.apply(Router, [
    '/getList',
    // authentication,
    // ...validations.update,
    asyncRouteWrapper(controllers.getList),
]);

module.exports = Router;
