const CryptoJS = require('crypto-js');
const bcrypt = require('bcryptjs');
const moment = require('moment');
const {
    getUserInfo,
    paginate,
    generateJWToken,
    getCounts,
    mysqlQuery,
    getUserInfoSingle,
} = require('../../../helpers/constFunc');
const Mongo = require('../../../models/mongo');

let usersModel = Mongo.users.model;

module.exports.onlineUserList = async (req, res) => {
    try {
        const { myInfo } = req;
        const { redis, mysql } = req.app.get('db');
        const onlineUsers = await redis.get('newChatableOnlineUsers');
        if (onlineUsers === null) {
            res.success.OK('onlinUserList', {
                datas: [],
                hasNext: false,
                count: 0,
                onlineUsers: [],
            });
            return true;
        }
        const objectArray = Object.entries(onlineUsers);
        const page = req.query.page ? parseInt(req.query.page, 10) : 1;
        const limit = req.query.limit ? parseInt(req.query.limit, 10) : 25;
        const comId = req.query.comId ? parseInt(req.query.comId, 10) : 0;
        const isCount = req.query.count ? parseInt(req.query.count, 10) : 0;
        const start = parseInt(page * limit, 10);
        const accessToken =
            req.cookies.accessToken || req.query.accessToken || req.headers['x-access-token'];
        const userIds = [];
        objectArray.map(([key, value]) => {
            if (value.com_id === myInfo.comId) userIds.push(value.id);
            return value;
        });

        if (isCount === 1) {
            res.success.OK('onlinUserList', {
                datas: [],
                hasNext: false,
                count: userIds.length,
                onlineUsers: [],
            });
            return true;
        }
        const usersList = await getUserInfo(userIds, redis, mysql, {
            accessToken,
            valueType: 'userId',
            comId: myInfo.comId,
        });
        const responseList = usersList.sort((a, b) => {
            if (a.position > b.position) return 1;
            if (a.position < b.position) return -1;
            return 0;
        });
        const hasNext = userIds.length - start > 0;
        const filterResponse = paginate(responseList, limit, page);

        const counts = await getCounts(myInfo.comId, userIds, mysql);
        const data = {
            datas: filterResponse,
            hasNext,
            count: responseList.length,
            offlineCount: counts.offlineCount,
            onlineCount: counts.onlineCount,
            onlineUserIds: userIds,
        };
        // const used = process.memoryUsage().heapUsed / 1024 / 1024;
        // console.log(`online users used ${Math.round(used * 100) / 100} MB`);
        res.success.OK('onlinUserList', data);
    } catch (e) {
        console.log('online_error', e);
        return { error: e };
    }
};
