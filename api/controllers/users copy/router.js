const Router = require('express').Router();

const { authentication } = require('../../middlewares');
const asyncRouteWrapper = require('../../../helpers/asyncRouteWrapper');
const controllers = require('./controllers.js');

Router.get.apply(Router, [
    '/online',
    authentication,
    // ...validations.update,
    asyncRouteWrapper(controllers.onlineUserList),
]);

module.exports = Router;
