const Router = require('express').Router();

// Хэрэглэгчтэй холбоотой хүсэлтүүд
Router.use('/users', require('./controllers/users/router'));

module.exports = Router;
