const { forOwn } = require('lodash');
const clc = require('cli-color');
const models = require('../models/mongo');

const errorLog = clc.red.bold;
const successLog = clc.green.bold;
const args = process.argv;

const command = args[2];
const modelName = args[3];

if (modelName === 'all') {
    forOwn(models, (key, value) => {
        try {
            if (value === 'asky' || value === '') return;

            const seeder = require(`./${value}`);

            if (command === 'create') {
                seeder.create(key.model);
            } else if (command === 'delete') {
                key.model
                    .deleteMany()
                    .then(() => {
                        console.log(successLog(value, 'data deleted')); // Success
                    })
                    .catch(e => {
                        console.log(errorLog(e)); // Failure
                    });
            }
        } catch (e) {
            console.log(errorLog(value, 'model notfound', e));
            process.exit(1);
        }
    });
} else {
    if (typeof models[modelName] === 'undefined') {
        console.log(errorLog(modelName, 'model notfound'));
        process.exit(1);
    }

    const seeder = require(`./${modelName}`);

    if (command === 'create') {
        seeder.create(models[modelName].model);
    } else if (command === 'delete') {
        models[modelName].model
            .deleteMany()
            .then(() => {
                console.log(successLog(modelName, 'data deleted')); // Success
            })
            .catch(e => {
                console.log(errorLog(e)); // Failure
            });
    }
}
