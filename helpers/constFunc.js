/* eslint-disable prettier/prettier */
/* eslint-disable no-restricted-syntax */
/* eslint-disable new-cap */
const fs = require('fs');
const CryptoJS = require('crypto-js');
const axios = require('axios');
// const os = require('os');
const moment = require('moment');
const debug = require('debug')('able:socket');
const config = require('../config/config');

function replaceAt(str, index, ch) {
    return str.replace(/./g, (c, i) => (i === index ? ch : c));
}
function encStr(Str) {
    const value = new Buffer.from(Str);

    const value1 = value.toString('base64');
    const len = value1.length;
    const a = Math.ceil(len / 3);
    const b = Math.ceil(len / 4);
    const cut1 = value1.substr(a, 1);
    const cut2 = value1.substr(b, 1);

    const paste1 = replaceAt(value1, b, cut1);
    const paste2 = replaceAt(paste1, a, cut2);
    return paste2;
}

module.exports = {
    encStr,
};
