// const fetch = require('node-fetch');
const axios = require('axios');

// const cloudCenterApi = 'https://mysql.able.mn:8000';

const cloudCenterApi = 'https://develop.able.mn:8989';

const requestCloudCenter = async (data, myInfo) => {
    const { token } = myInfo;
    return axios({
        method: 'get',
        url: `${cloudCenterApi}/api/onlineUsers/redisUserInfo?userId=${data}&accessToken=${token}`,
        headers: {
            'Content-Type': 'application/json',
            'x-access-token': token,
        },
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log('aldaa', err);
        });
};

const getImageType = async type => {
    switch (type) {
        case 'image/gif':
            return true;
        case 'image/pjpeg':
            return true;
        case 'image/jpeg':
            return true;
        case 'image/jpg':
            return true;
        case 'image/png':
            return true;
        case 'image/x-png':
            return true;
        default:
            return false;
    }
};

module.exports = {
    requestCloudCenter,
    getImageType,
}
