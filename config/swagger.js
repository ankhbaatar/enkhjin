const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
        title: 'CHAT project api document',
        version: '1.0.0',
        description: '',
        license: {
            name: 'ABLESOFT',
            url: '',
        },
        contact: {
            name: 'CHAT TEAM',
            url: '',
        },
    },
    components: {
        securitySchemes: {
            bearerAuth: {
                type: 'http',
                scheme: 'bearer',
                bearerFormat: 'JWT',
            },
        },
    },
    security: {
        '- bearerAuth': '[]',
    },
    tags: [
        {
            name: 'Rooms',
            description: 'Rooms endPoint',
        },
    ],
    consumes: ['application/json'],
    produces: ['application/json'],
    servers: [
        {
            url: 'http://localhost:5000/api',
            description: 'Development server',
        },
        {
            url: 'http://103.17.108.247:8989/api',
            description: 'production server',
        },
    ],
};

const options = {
    swaggerDefinition,
    apis: ['api/controllers/rooms/_docs.js', 'api/controllers/user/_docs.js'],
};

module.exports = options;
