require('dotenv').config();

const config = {
    app: {
        port: parseInt(process.env.APP_PORT, 10) || 5000,
    },
    user: {
        // authkey: process.env.AUTH_KEY || 'bytelinks!@#',
    },
    db: {
        mongo: {
            host: process.env.MONGO_HOST,
            user: process.env.MONGO_USER,
            pass: process.env.MONGO_PASS,
        },
        redis: {
            host: process.env.REDIS_HOST,
            port: 6379,
        },
    },
};

module.exports = config;
