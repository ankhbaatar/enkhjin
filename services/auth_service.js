const debug = require('debug')('able:auth');
const jwt = require('jsonwebtoken');

function parseCookie(request) {
    const list = {};
    const rc = request.headers.cookie;

    if (rc) {
        rc.split(';').forEach(cookie => {
            const parts = cookie.split('=');
            list[parts.shift().trim()] = unescape(parts.join('='));
        });
    }

    return list;
}

module.exports = (socket, next) => {
    // console.log('Cookies: ', socket.cookies);

    const cookie = parseCookie(socket.handshake);

    if (!socket.handshake.query || !socket.handshake.headers) {
        next(new Error('A bad request'));
        console.log('error', 'a bad request');
        return false;
    }

    const accessToken =
        cookie.accessToken ||
        socket.handshake.query.accessToken ||
        socket.handshake.headers['x-access-token'];
    if (!accessToken) {
        next(new Error('undefined token'));
        console.log('error', 'undefined accessToken');
        return false;
    }

    try {
        const tokenData = jwt.verify(accessToken, 'EnkjinKeyData');
        if (!tokenData.myHID) {
            next(new Error('undefined myHID'));
            console.log('error', 'undefined myHID');
        }
        if (!tokenData.myComId) {
            next(new Error('undefined myComId'));
            console.log('error', 'undefined myComId');
        }
        if (!tokenData.key) {
            next(new Error('undefined key'));
            console.log('error', 'undefined key');
        }
        debug(tokenData);

        // accept connection
        socket.type = socket.handshake.query.type || 'web';
        socket.user = tokenData;
        socket.token = accessToken;
        // console.log(`successfuly auth connected user:${tokenData.myHID}`);
        next();
    } catch (err) {
        next(err);
        return false;
    }
};
