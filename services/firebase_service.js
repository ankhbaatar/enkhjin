const axios = require('axios');

module.exports.send = async (ndata, tokens, os) => {
    const message = {
        to: tokens,
        priority: 'high',
        data: ndata,
    };
    if (os === 'ios') {
        message.notification = ndata;
        message.aps = {
            sound: 'whoosh.wav',
            badge: 3,
            title: 'new message',
            body: 'Hello World!',
        };
    }
    axios({
        method: 'POST',
        url: process.env.FIREBASE_URL,
        data: message,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `key=${process.env.FIREBASE_KEY}`,
        },
    });
};
