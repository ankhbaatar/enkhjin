const request = require('supertest');
const app = require('../server');

beforeAll(done => {
    done();
});

afterAll(done => {
    done();
});
describe('Room List Endpoints', () => {
    it('RoomList', async () => {
        const res = await request(app).get('/api/rooms?userId=4497&limit=20&showCount=1').set({
            'x-access-token':
                'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJteUhJRCI6MjA2MjMxOSwibXlDb21JZCI6Miwia2V5IjoiYWJsZXNvZnQiLCJicm93c2VyIjoiQ2hyb21lIiwiYnJvd3NlclZlciI6IjkxLjAuNDQ3Mi43NyIsIm9zIjoiTWFjIE9TIFgiLCJvc1R5cGUiOm51bGwsImxvY2F0aW9uIjoiVWxhbiBCYXRvcixNTig0Ny45MDc3LDEwNi44ODMyKSIsImNsaWVudElQIjoiMjAyLjEzMS4yMzYuMTQiLCJjbGllbnRUeXBlIjoid2ViIiwic2NyZWVuV2lkdGgiOjE0NDAsInNjcmVlbkhlaWdodCI6OTAwLCJpYXQiOjE2MjMyOTk4ODN9.Mkj64dc-UJ97RWUTagP8BKGabXiBVeA2jN8Ppff9lv8',
            Accept: 'application/json',
        });
        expect(res.statusCode).toEqual(200);
        expect.objectContaining(res.body.data);
    });
    it('RoomCreate', async () => {
        const res = await request(app)
            .post('/api/rooms')
            .set({
                'x-access-token':
                    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJteUhJRCI6MjA2MjMxOSwibXlDb21JZCI6Miwia2V5IjoiYWJsZXNvZnQiLCJicm93c2VyIjoiQ2hyb21lIiwiYnJvd3NlclZlciI6IjkxLjAuNDQ3Mi43NyIsIm9zIjoiTWFjIE9TIFgiLCJvc1R5cGUiOm51bGwsImxvY2F0aW9uIjoiVWxhbiBCYXRvcixNTig0Ny45MDc3LDEwNi44ODMyKSIsImNsaWVudElQIjoiMjAyLjEzMS4yMzYuMTQiLCJjbGllbnRUeXBlIjoid2ViIiwic2NyZWVuV2lkdGgiOjE0NDAsInNjcmVlbkhlaWdodCI6OTAwLCJpYXQiOjE2MjMyOTk4ODN9.Mkj64dc-UJ97RWUTagP8BKGabXiBVeA2jN8Ppff9lv8',
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            })
            .field('name', 'TEST')
            .field('users', JSON.stringify([{ id: 4497 }, { id: 2062319 }]));
        expect.objectContaining(res.body.data);
        expect(res.statusCode).toEqual(200);
    });
});

// describe('RoomCreate Endpoints', () => {
//     it('RoomCreate', async () => {
//         const res = await request(app)
//             .post('api/rooms')
//             .type('form-')
//             .set({
//                 Accept: 'application/json',
//                 'x-access-token':
//                     'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJteUhJRCI6MjA2MjMxOSwibXlDb21JZCI6Miwia2V5IjoiYWJsZXNvZnQiLCJicm93c2VyIjoiQ2hyb21lIiwiYnJvd3NlclZlciI6IjkxLjAuNDQ3Mi43NyIsIm9zIjoiTWFjIE9TIFgiLCJvc1R5cGUiOm51bGwsImxvY2F0aW9uIjoiVWxhbiBCYXRvcixNTig0Ny45MDc3LDEwNi44ODMyKSIsImNsaWVudElQIjoiMjAyLjEzMS4yMzYuMTQiLCJjbGllbnRUeXBlIjoid2ViIiwic2NyZWVuV2lkdGgiOjE0NDAsInNjcmVlbkhlaWdodCI6OTAwLCJpYXQiOjE2MjMyOTk4ODN9.Mkj64dc-UJ97RWUTagP8BKGabXiBVeA2jN8Ppff9lv8',
//             })
//             .send({
//                 name: 'ankhaa',
//                 users: JSON.stringify([{ id: 4497 }, { id: 2062319 }]),
//                 roomType: 0,
//             });
//         expect(res.statusCode).toEqual(200);
//         expect(res.body).toHaveProperty('token');
//     });
// });

// describe("Login Endpoints", () => {
//     it("Login password invalid", async () => {
//         const res = await request(app).post("/api/users/login").send({
//             phone: "86088806",
//             password: "123shdjk"
//         });
//         expect(res.statusCode).toEqual(200);
//         expect(res.body).toHaveProperty("error");
//     });
// });

// describe("Login Endpoints", () => {
//     it("Login phone invalid", async () => {
//         const res = await request(app).post("/api/users/login").send({
//             phone: "8608880656",
//             password: "123"
//         });
//         expect(res.statusCode).toEqual(200);
//         expect(res.body).toHaveProperty("error");
//     });
// });
