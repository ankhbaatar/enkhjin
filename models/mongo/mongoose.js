/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-console */

const mongoose = require('mongoose');
const config = require('../../config/config.js');

const mongoURL = new URL(config.db.mongo.host);

const Mongoose = mongoose.createConnection(mongoURL.href, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

Mongoose.on('error', console.error.bind(console, 'Mongoose connection error:'));

module.exports = Mongoose;
