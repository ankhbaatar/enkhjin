const Mongoose = require('mongoose');
const mongoose = require('./mongoose');

const NAME = 'users';

/* Бүх өрөөнүүд */
const usersSchema = new Mongoose.Schema(
    {
        _id: {
            type: Mongoose.Schema.ObjectId,
            required: true,
            auto: true,
        },
        userName: { type: String },
        userPass: { type: String },
    },
    {
        timestamps: true,
        versionKey: false,
    }
);

module.exports = {
    name: NAME,
    model: mongoose.model(NAME, usersSchema),
};
