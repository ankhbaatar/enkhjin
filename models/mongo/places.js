/* eslint-disable no-dupe-keys */

const Mongoose = require('mongoose');
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');
const mongoose = require('./mongoose');

const NAME = 'projects';
const types = {
    TEXT: 0,
    FILE: 1,
};

const states = {
    PRIVATE: 0,
    GROUP: 1,
};

const fileType = {
    IMG: 0,
    OTHER: 1,
};

const commentsSchema = new Mongoose.Schema({
    _id: {
        type: Mongoose.Schema.ObjectId,
        required: true,
        auto: true,
    },
    filePath: { type: String },
    fileName: { type: String },
    createdAt: { type: Date, default: Date.now },
});


/* Бүх өрөөнүүд */
const placesSchema = new Mongoose.Schema(
    {
        _id: {
            type: Mongoose.Schema.ObjectId,
            required: true,
            auto: true,
        },
        title: { type: String, required: true },
        body: { type: String, required: true },
        path: { type: String, required: true },
        comments: [commentsSchema],
    },
    {
        timestamps: true,
        versionKey: false,
    }
);

placesSchema.plugin(aggregatePaginate);

module.exports = {
    types,
    states,
    fileType,
    name: NAME,
    model: mongoose.model(NAME, placesSchema),
};
