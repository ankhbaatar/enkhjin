module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('users', {
            id: {
                type: Sequelize.INTEGER,
                field: 'id',
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            email: {
                type: Sequelize.STRING,
                field: 'email',
                allowNull: true,
            },
            phone: {
                type: Sequelize.STRING,
                field: 'phone',
                allowNull: false,
            },
            name: {
                type: Sequelize.STRING,
                field: 'name',
                allowNull: false,
            },
            type: {
                type: Sequelize.STRING,
                field: 'type',
                allowNull: false,
            },
            password: {
                type: Sequelize.STRING,
                field: 'password',
                allowNull: false,
            },
            avatar_path: {
                type: Sequelize.STRING,
                field: 'avatar_path',
                allowNull: true,
            },
            confirmed_at: {
                type: Sequelize.DATE,
                field: 'confirmed_at',
                allowNull: true,
            },
            created_at: {
                type: Sequelize.DATE,
                field: 'created_at',
                allowNull: false,
                defaultValue: Sequelize.NOW,
            },
            updated_at: {
                type: Sequelize.DATE,
                field: 'updated_at',
                allowNull: false,
                defaultValue: Sequelize.NOW,
            },
        });
    },
    down: queryInterface => {
        return queryInterface.dropTable('users');
    },
};
