/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
const fs = require('fs');
const cors = require('cors');
const http = require('http');
const https = require('https');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const responseHandler = require('express-response-handler');
const constants = require('constants');
const os = require('os');
const cluster = require('cluster');

const clusterWorkerSize = os.cpus().length;
const CryptoJS = require('crypto-js');

// Sentry requires
const Sentry = require('@sentry/node');
const Tracing = require('@sentry/tracing');
const { setupMaster, setupWorker } = require('@socket.io/sticky');
// const eiows = require('eiows');
// const Cron = require('node-cron');

// swagger дээр api дуудах заавар оруулах тохиргоо
const swaggerAuthOptions = {
    swaggerOptions: {
        authAction: {
            Bearer: {
                schema: {
                    type: 'http',
                    in: 'header',
                    name: 'Authorization',
                },
                name: 'Bearer',
                value:
                    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6Ijg4ODkxODAzIiwiYWNjb3VudElkIjozMiwidWFnZW50IjoiUG9zdG1hblJ1bnRpbWUvNy4yNi4xMCIsImlhdCI6MTYxNjkyNDgwMCwiZXhwIjoxNjE3Nzg4ODAwfQ.aWsqw48tquJsnt3styia_akbpeiiyNlNIe5YQ3GrJkE',
            },
        },
    },
    host: 'http://localhost:5000/docs/#/',
};

const swaggerJSDoc = require('swagger-jsdoc'); 
const swaggerUi = require('swagger-ui-express');

// config файл холболт
const config = require('./config/config.js');
// const swaggerOptions = require('./config/swagger.js');
const responseCode = require('./config/response');

const swaggerDocument = require('./swagger.json');

const appRoot = path.resolve(__dirname);

// variable defines

// server-н cors зөвшөөрөх
const corsOptions = {
    origin: '*',
    credentials: true,
};

// server-н redis-тэй холболт үүсгэх тохиргоо
const redis = require('./models/redis');

// тохиргоонуудыг db key дээр нэгтгэх
const redisConf = redis(config.db.redis);

const db = {
    redis: redisConf,
};
const app = express();

app.set('db', db);
app.set('appRoot', appRoot);
app.use(cors(corsOptions));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json({ extended: true, limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true }));

// Алдаа Sentry дээр барьж авах
// Sentry.init({
//     dsn: 'end path bainaa',
//     integrations: [
//         // enable HTTP calls tracing
//         new Sentry.Integrations.Http({ tracing: true }),
//         // enable Express.js middleware tracing
//         new Tracing.Integrations.Express({ app }),
//     ],

//     // We recommend adjusting this value in production, or using tracesSampler
//     // for finer control
//     tracesSampleRate: 1.0,
// });

if (process.env.NODE_ENV === 'production') {
    // app.use(Sentry.Handlers.requestHandler());
    // app.use(Sentry.Handlers.tracingHandler());
}
// хариу буцаах global handler
app.use(responseHandler(responseCode));

// Үндсэн RestApi router
app.use('/api', require('./api/routes'));

// documentations
// app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, swaggerAuthOptions));
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, swaggerAuthOptions));

// Холбогдох router олдоогүй тохиолдол
app.use((req, res) => {
    res.error.User('not_found.error');
});

// Error handler
// app.use(Sentry.Handlers.errorHandler());
app.use((err, req, res, next) => {
    res.error.ServerError('server.error', err);
});

// http сервер үүгсэх
let server;
// http сервер дээр SSL тохируулах
if (process.env.NODE_ENV === 'production') {
    let ssl = {
        secureProtocol: 'SSLv23_method',
        secureOptions: constants.SSL_OP_NO_SSLv3,
        ca: fs.readFileSync(''),
        key: fs.readFileSync(''),
        cert: fs.readFileSync(''),
    };

    server = https.createServer(ssl, app);
} else {
    server = http.Server(app);
}
// Node multiThread-р ажилуулах thread тус бүр дээр ачаалалыг хуваах
process.setMaxListeners(0);
if (clusterWorkerSize > 1) {
    if (cluster.isMaster) {
        setupMaster(server, {
            loadBalancingMethod: 'round-robin',
        });
        cluster.setupMaster({
            serialization: 'advanced',
        });
        let time = 0;
        for (let i = 0; i < clusterWorkerSize; i++) {
            time = i;
            cluster.fork();
        }
        cluster.on('exit', worker => {
            console.log('Worker', worker.id, ' has exitted.');
        });
    } else {
        // Node http сэрвер сонсох хаяг process id
        server.listen(config.app.port, () => {
            console.log(
                `Express server listening on port ${config.app.port} and worker ${process.pid}`
            );
        });
    }
} else {
    server.listen(config.app.port, () => {
        console.log(`web server listening on --> : ${config.app.port}`);
    });
}
module.exports = app;
